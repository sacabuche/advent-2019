package a01

import (
	"fmt"
	"testing"
)

func TestRequiredFuel(t *testing.T) {
	cases := []struct {
		input int64
		exp   int64
	}{
		{input: 12, exp: 2},
		{input: 14, exp: 2},
		{input: 1969, exp: 654},
		{input: 100756, exp: 33583},
	}

	for _, kase := range cases {
		t.Run(fmt.Sprintf("kase.input"), func(t *testing.T) {
			got := RequiredFuel(kase.input)
			if kase.exp != got {
				t.Fatalf("Bad result exp: %d got: %d", kase.exp, got)
			}
		})
	}

}
